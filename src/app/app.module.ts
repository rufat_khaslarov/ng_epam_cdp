import { BrowserModule } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { AlertModule, CollapseModule } from 'ngx-bootstrap';

import { LayoutModule } from './layout/layout.module';
import { AuthModule } from './auth/auth.module';
import { CourseModule } from './course/course.module';
import { SearchModule } from './search/search.module';
import { BreadcrumbsModule } from './breadcrumbs/breadcrumbs.module';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { reducers, metaReducers } from './core/store/reducers';
import { AuthEffects, CourseEffects } from './core/store/effects';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    AlertModule.forRoot(),
    CollapseModule.forRoot(),
    BrowserModule,
    NoopAnimationsModule,
    LayoutModule,
    CourseModule,
    SearchModule,
    BreadcrumbsModule,
    AuthModule,
    CoreModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot([
      AuthEffects,
      CourseEffects
    ]),
    StoreDevtoolsModule.instrument({
      maxAge: 10
    })
  ],
  providers: [
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {

}
