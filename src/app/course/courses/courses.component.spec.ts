import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CoursesService } from '../../core/services';
import { CoursesComponent } from './courses.component';
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('CoursesComponent', () => {

  let component: CoursesComponent;
  let fixture: ComponentFixture<CoursesComponent>;
  let coursesService: Partial<CoursesService>;

  beforeEach(() => {

    coursesService = { getCourses: jasmine.createSpy('getCourses').and.returnValue(['test']) };

    TestBed.configureTestingModule({
      declarations: [ CoursesComponent ],
      schemas: [ NO_ERRORS_SCHEMA ],
      providers: [
        { provide: CoursesService, useValue: coursesService }
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should call CoursesService getCourses method onInit', () => {
    expect(coursesService.getCourses).toHaveBeenCalled();
    expect(component.courses.length).toBe(1);
  });

  it('should has courses and load more button', () => {
    let el: HTMLElement = fixture.nativeElement;
    expect(el.querySelector('.courses')).toBeTruthy();
    expect(el.querySelector('.courses__load_more')).toBeTruthy();
  });

  it('should call onLoadMore by loadMore button clicking', async(() => {
    spyOn(component, 'onLoadMore');

    let button: DebugElement = fixture.debugElement.query(By.css('button'));
    button.triggerEventHandler('click', undefined);

    fixture.whenStable().then(() => {
      expect(component.onLoadMore).toHaveBeenCalled();
    });
  }));

  it('should call console.log in onDelete and onLoadMore', () => {
    spyOn(console, 'log');
    component.onDelete('1');
    expect(console.log).toHaveBeenCalled();
    component.onLoadMore();
    expect(console.log).toHaveBeenCalled();
  });
});
