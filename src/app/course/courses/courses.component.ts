import {Component, OnDestroy, OnInit} from '@angular/core';
import { CoursesService, SearchService } from '../../core/services';
import { Course } from '../../shared/models';
import { OrderByDatePipe } from '../../shared/pipes/order-by-date.pipe';
import { FilterByNamePipe } from '../../shared/pipes/filter-by-name.pipe';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ConfirmationModalComponent } from '../../shared/confirmation-modal/confirmation-modal.component';
import { Router } from '@angular/router';
import { Subscription} from 'rxjs/index';
import {AppState, getCoursesList, getCoursesSearchValue, getCourseState} from '../../core/store/reducers';
import {select, Store} from '@ngrx/store';
import * as CourseActions from '../../core/store/actions/course.actions';
import { Actions, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';

const COURSES_PER_PAGE: number = 5;

@Component({
  selector: 'courses-list',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss'],
  providers: [OrderByDatePipe, FilterByNamePipe]
})
export class CoursesComponent implements OnInit, OnDestroy {

    private data: Course[];
    private currentPage: number = 0;

    private loadCoursesSub: Subscription;
    private deleteCourseSub: Subscription;

    public bsModalRef: BsModalRef;
    public courses: Course[];

    constructor(
      private orderByDatePipe: OrderByDatePipe,
      private filterByNamePipe: FilterByNamePipe,
      private modalService: BsModalService,
      private router: Router,
      private store: Store<AppState>,
      private actions: Actions
    ) {
        this.data = [];
        this.courses = [];
    }

    private loadMoreCourses(): void {
      this.store.dispatch(new CourseActions.Load({
        page: this.currentPage,
        perPage: COURSES_PER_PAGE
      }));
    }

    private reloadCourses(): void {
      this.store.dispatch(new CourseActions.Load({
        page: 0,
        perPage: this.currentPage * COURSES_PER_PAGE
      }));
    }

    public ngOnInit(): void {

        this.loadMoreCourses();

        this.loadCoursesSub = this.store.pipe(
            select(getCourseState)
        ).subscribe(({ entities, searchValue }) => {
            this.currentPage = !searchValue ? this.currentPage + 1 : 0;
            this.data = entities;
            this.courses = this.data.slice();
        });

        this.deleteCourseSub = this.actions.pipe(
            ofType(CourseActions.CourseActionTypes.DeleteSuccess),
            tap(() => this.reloadCourses())
        ).subscribe();
    }

    public onLoadMore(): void {
        this.loadMoreCourses();
    }

    public onEdit(id: string): void {
        this.store.dispatch(new CourseActions.Edit(Number(id)));
    }

    public onRemove(id: string): void {

      const initialState: object = {
        onConfirm: () => this.store.dispatch(new CourseActions.Delete(Number(id)))
      };

      this.bsModalRef = this.modalService.show(ConfirmationModalComponent, {initialState});
    }

    public ngOnDestroy(): void {
      this.loadCoursesSub.unsubscribe();
      this.deleteCourseSub.unsubscribe();
    }
}
