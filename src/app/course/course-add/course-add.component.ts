import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Subscription } from 'rxjs/index';

import { Course, Author } from '../../shared/models';
import { regExpValidator } from '../../shared/directives/regexp-validator.directive';
import { CoursesService } from '../../core/services';
import {
    AppState,
    getCoursesList,
    getCoursesSearchValue,
    getCourseState,
    getCoursesSelectedItem,
    getCoursesAuthorsList
} from '../../core/store/reducers';
import * as CourseActions from '../../core/store/actions/course.actions';

@Component({
  selector: 'course-add',
  templateUrl: './course-add.component.html',
  styleUrls: ['./course-add.component.scss']
})
export class CourseAddComponent implements OnInit, OnDestroy {

  private onParamChangeSub: Subscription;
  private loadAuthorsSub: Subscription;
  public courseForm: FormGroup = new FormGroup({
    name: new FormControl('', [ Validators.required, Validators.maxLength(50) ]),
    description: new FormControl('', [ Validators.required, Validators.maxLength(500) ]),
    date: new FormControl('', [ Validators.required, regExpValidator('^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$') ]),
    length: new FormControl('', [ Validators.required, regExpValidator('^[0-9]*$') ]),
    authors: new FormControl([], [ Validators.required ]),
  });
  public submitted: boolean = false;
  public authors: Author[] = [];

  constructor(
      private router: Router,
      private store: Store<AppState>
  ) {}

  get f(): { [key: string]: AbstractControl } { return this.courseForm.controls; }

  public ngOnInit(): void {

      this.store.dispatch(new CourseActions.LoadAuthors());

      this.onParamChangeSub = this.store.pipe(
          select(getCoursesSelectedItem)
      )
      .subscribe(data => data && this.courseForm.patchValue(data));

      this.loadAuthorsSub = this.store.pipe(
          select(getCoursesAuthorsList)
      ).subscribe(data => {
          if (data && data.length) {
              this.authors = data;
          }
      });
  }

  public onSubmit(event: MouseEvent): void {
      console.log(this.courseForm.value, this.courseForm);

      this.submitted = true;

      if (this.courseForm.invalid) {
          return;
      }

      this.store.dispatch(new CourseActions.Update(this.courseForm.value));
  }

  public onCancel(event: MouseEvent): void {
    event.preventDefault();
    this.router.navigate(['courses']);
  }

  public ngOnDestroy(): void {
    this.onParamChangeSub.unsubscribe();
    this.loadAuthorsSub.unsubscribe();
  }
}
