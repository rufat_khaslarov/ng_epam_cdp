import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { faStar, faTimes, faEdit, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { Course } from '../../shared/models';

@Component({
  selector: 'course-item',
  templateUrl: './course-item.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./course-item.component.scss']
})
export class CourseItemComponent {

  @Input()
  public course: Course;
  @Output()
  public courseItemRemove: EventEmitter<string> = new EventEmitter<string>();
  @Output()
  public courseItemEdit: EventEmitter<string> = new EventEmitter<string>();

  public faStar: IconDefinition = faStar;
  public faTimes: IconDefinition = faTimes;
  public faEdit: IconDefinition = faEdit;

  public onRemove(id: string): void {
    this.courseItemRemove.emit(id);
  }

  public onEdit(id: string): void {
    this.courseItemEdit.emit(id);
  }

}
