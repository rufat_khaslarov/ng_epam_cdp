import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {Component, DebugElement} from '@angular/core';
import { Course } from '../../shared/models';
import { CourseItemComponent } from './course-item.component';
import {By} from '@angular/platform-browser';

@Component({
  template: `
    <course-item
      [course]=course (deleted)="onDelete($event)">
    </course-item>`
})
class TestHostComponent {
  public course: Course = {
    id: 'uuid',
    title: 'Test course',
    description: 'Test course description',
    createdAt: 150000,
    duration: 300,
    topRated: false
  };
  public deletedId: number;
  public onDelete(id: number): void { this.deletedId = id; }
}

describe('CourseItemComponent', () => {

  let testHostcomponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseItemComponent, TestHostComponent ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    testHostcomponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should call onDeleted by clicking delete button', async(() => {

    let button: DebugElement = fixture.debugElement.query(By.css('button.course__actions_delete'));
    button.triggerEventHandler('click', undefined);

    fixture.whenStable().then(() => {
      expect(testHostcomponent.deletedId).toEqual(1);
    });
  }));

  it('should render info from course input', () => {
    let course: HTMLElement = fixture.nativeElement.querySelector('.course');
    expect(course.querySelector('.course__header_left').textContent.trim()).toEqual('Test course');
    expect(course.querySelector('.course__header_right').textContent.trim()).toEqual('300 12/12/12');
    expect(course.querySelector('.course__desc').textContent.trim()).toEqual('Test course description');
  });

});
