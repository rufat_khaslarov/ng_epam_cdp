import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ButtonsModule } from 'ngx-bootstrap';

import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { CoursesComponent } from './courses/courses.component';
import { CourseItemComponent } from './course-item/course-item.component';
import { CourseAddComponent } from './course-add/course-add.component';
import { CourseRoutingModule } from './course-routing.module';

@NgModule({
  imports: [
    ButtonsModule.forRoot(),
    CommonModule,
    CoreModule,
    SharedModule,
    FontAwesomeModule,
    FormsModule,
    CourseRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [
    CoursesComponent,
    CourseItemComponent,
    CourseAddComponent
  ],
  providers: [],
  exports: [
    CoursesComponent,
    CourseItemComponent
  ]
})
export class CourseModule {}
