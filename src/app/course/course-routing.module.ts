import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CoursesComponent } from './courses/courses.component';
import { CourseAddComponent } from './course-add/course-add.component';
import { AuthGuard } from '../core/guards';

const courseRoutes: Routes = [
  { path: 'courses',  component: CoursesComponent, canActivate: [AuthGuard]  },
  { path: 'courses/add', component: CourseAddComponent, canActivate: [AuthGuard] },
  { path: 'courses/:id', component: CourseAddComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(courseRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class CourseRoutingModule {}
