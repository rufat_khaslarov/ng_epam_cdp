import { Injectable } from '@angular/core';
import { Course } from '../../shared/models';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import {map, first, flatMap, filter, tap} from 'rxjs/operators';
import { of } from 'rxjs/index';
const BASE_URL: string = 'http://localhost:3004/courses';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {
  private http: HttpClient;
  private defaultValue: Course = {
    id: 0,
    name: '',
    description: '',
    date: new Date(),
    length: 0,
    isTopRated: false,
    authors: []
  };

  constructor(http: HttpClient) {
    this.http = http;
  }

  public getCourses(page: number, count: number): Observable<Course[]> {
    const start: number = page * count;
    return this.http.get<Course[]>(BASE_URL, {
      params: {
        start: start.toString(),
        count: count.toString()
      }
    })
    .pipe(
      map(data => {
        return data.map((course: Course) => {
          return {
            ...course,
            date: new Date(course.date)
          };
        });
      }),
      filter(data => data && data.length > 0)
    );
  }

  public getCourseById(id: number): Observable<Course>  {

    if (isNaN(id)) {
      return of({ ...this.defaultValue});
    }

    return this.http.get(BASE_URL)
      .pipe(
        flatMap(response => response as Course[]),
        map(data => ({...data, date: new Date(data.date)})),
        first(data => data.id === id, {...this.defaultValue})
      );
  }

  public getCourseByDesc(textFragment: string): Observable<Course[]> {
    return this.http.get<Course[]>(BASE_URL, {
      params: {
        textFragment
      }
    })
    .pipe(
      map(data => {
        return data.map((course: Course) => {
          return {
            ...course,
            date: new Date(course.date)
          };
        });
      })
    );
  }

  public removeCourse(id: number): Observable<Course> {
    const url: string = `${BASE_URL}/${id}`;
    return this.http.delete<Course>(url);
  }

  public createOrUpdateCourse(data: Course): Observable<Course> {
    const course: Course = {
        ...this.defaultValue,
        ...data,
        date: new Date(data.date)
    };
    if (course.id && course.id >= 0) {
      return this.http.put<Course>(`${BASE_URL}/${course.id}`, course);
    } else {
      return this.http.post<Course>(BASE_URL, course);
    }
  }
}
