import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private searchValue: string = '';
  public searchValueUpdated: EventEmitter<string> = new EventEmitter(); // TODO: use observable

  public setSearchValue(value: string): void {
    this.searchValue = value;
    this.searchValueUpdated.emit(this.searchValue);
  }
}
