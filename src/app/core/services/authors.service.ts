import { Injectable } from '@angular/core';
import { Course, Author } from '../../shared/models';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import {map, first, flatMap, filter, tap} from 'rxjs/operators';
import { of } from 'rxjs/index';
const BASE_URL: string = 'http://localhost:3004/authors';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {

  constructor(private http: HttpClient) {}

  public getAuthors(): Observable<Author[]> {
    return this.http.get<Author[]>(BASE_URL)
    .pipe(
      filter(data => data && data.length > 0)
    );
  }
}
