import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private isLoading: BehaviorSubject<boolean>;

  constructor() {
    this.isLoading = new BehaviorSubject<boolean>(false);
  }

  public setStatus(status: boolean): void {
    this.isLoading.next(status);
  }

  public getStatus(): Observable<boolean> {
    return this.isLoading.asObservable();
  }
}
