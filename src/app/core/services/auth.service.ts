import { Injectable } from '@angular/core';
import { User } from '../../shared/models';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { first, tap, flatMap, filter, map } from 'rxjs/operators';

const BASE_URL: string = 'http://localhost:3004/users';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private http: HttpClient;
  private authToken: string = '';
  private userDataUpdates: BehaviorSubject<User>;

  constructor(http: HttpClient) {
    this.http = http;
    this.userDataUpdates = new BehaviorSubject<User>(undefined);
  }

  public getToken(): string {
    return this.authToken;
  }

  public login(login: string, password: string): Observable<User>  {
    return this.http.get<User[]>(BASE_URL, {
      params: {
        login,
        password
      }
    }).pipe(
      flatMap(response => response as User[]),
      filter(data => !!data),
      first(),
      tap((data) => {
        this.authToken = data.fakeToken;
        this.userDataUpdates.next(data);
      })
    );
  }

  public getUserDataUpdates(): Observable<User> {
    return this.userDataUpdates.asObservable();
  }

  public logOut(): void {
    this.authToken = '';
    this.userDataUpdates.next(undefined);
  }

  public isAuthenticated(): Observable<boolean> {
    return this.getUserDataUpdates().pipe(
      map((data) => !!data)
    );
  }
}
