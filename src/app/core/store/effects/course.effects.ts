import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { switchMap, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';
import { Course, CoursePagination } from '../../../shared/models/index';
import {
    CourseActions,
    CourseActionTypes,
    Load,
    LoadSuccess,
    Search,
    SearchSuccess,
    Delete,
    DeleteSuccess,
    Update,
    UpdateSuccess,
    Edit,
    LoadAuthors,
    LoadAuthorsSuccess
} from '../actions/course.actions';
import { CoursesService, AuthorsService } from '../../services/index';
import { Router } from '@angular/router';

@Injectable()
export class CourseEffects {

  @Effect()
  public load: Observable<Action> = this.actions.pipe(
    ofType<Load>(CourseActionTypes.Load),
    map(action => action.payload),
    switchMap((payload: CoursePagination) => {
      return this.courseService.getCourses(payload.page, payload.perPage)
        .pipe(
          map(data => new LoadSuccess(data))
        );
    })
  );

  @Effect({ dispatch: false })
  public edit: Observable<Action> = this.actions.pipe(
    ofType<Edit>(CourseActionTypes.Edit),
    tap((action: Edit) => this.router.navigate(['courses', action.payload]))
  );

  @Effect()
  public search: Observable<Action> = this.actions.pipe(
    ofType<Search>(CourseActionTypes.Search),
    map(action => action.payload),
    switchMap((payload: string) => {
      return this.courseService.getCourseByDesc(payload)
        .pipe(
          map(data => new SearchSuccess(data))
        );
    })
  );

  @Effect()
  public delete: Observable<Action> = this.actions.pipe(
    ofType<Delete>(CourseActionTypes.Delete),
    map(action => action.payload),
    switchMap((payload: number) => {
      return this.courseService.removeCourse(payload)
        .pipe(
          map(() => new DeleteSuccess())
        );
    })
  );

  @Effect()
  public update: Observable<Action> = this.actions.pipe(
    ofType<Update>(CourseActionTypes.Update),
    map(action => action.payload),
    switchMap((payload: Course) => {
      return this.courseService.createOrUpdateCourse(payload)
        .pipe(
          map(() => new UpdateSuccess()),
          tap(() => this.router.navigate(['courses']))
        );
    })
  );

  @Effect()
  public loadAuthors: Observable<Action> = this.actions.pipe(
    ofType<LoadAuthors>(CourseActionTypes.LoadAuthors),
    switchMap(() => {
      return this.authorService.getAuthors()
        .pipe(
           map(data => new LoadAuthorsSuccess(data))
        );
    })
  );

  constructor(
      private actions: Actions,
      private courseService: CoursesService,
      private authorService: AuthorsService,
      private router: Router) {}
}
