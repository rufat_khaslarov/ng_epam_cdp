import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { switchMap, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';

import { AuthActionTypes, Login, LoginSuccess } from '../actions/auth.actions';
import { Authenticate } from '../../../shared/models/index';
import { AuthService } from '../../services/index';

@Injectable()
export class AuthEffects {

  @Effect()
  public login: Observable<Action> = this.actions.pipe(
    ofType<Login>(AuthActionTypes.Login),
    map(action => action.payload),
    switchMap((auth: Authenticate) => {
      return this.authService.login(auth.login, auth.password)
        .pipe(
          map(user => new LoginSuccess({ user }))
        );
    })
  );

  @Effect({ dispatch: false })
  public loginSuccess: Observable<Action> = this.actions.pipe(
    ofType(AuthActionTypes.LoginSuccess),
    tap(() => this.router.navigate(['']))
  );

  @Effect({ dispatch: false })
  public loginRedirect: Observable<Action> = this.actions.pipe(
    ofType(AuthActionTypes.LoginRedirect, AuthActionTypes.Logout),
    tap(authed => {
      this.router.navigate(['/login']);
    })
  );

  constructor(private actions: Actions, private authService: AuthService, private router: Router) {}
}
