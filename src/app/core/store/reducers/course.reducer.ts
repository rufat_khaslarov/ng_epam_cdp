import { Course, Author } from '../../../shared/models/index';
import { CourseActions, CourseActionTypes } from '../actions/course.actions';
import { Selector, createSelector } from '@ngrx/store';

export interface State {
  entities: Course[];
  authors: Author[];
  searchValue: string;
  selectedId: number;
}

export const initialState: State = {
  entities: [],
  authors: [],
  searchValue: '',
  selectedId: undefined
};

export function reducer(state: State = initialState, action: CourseActions): State {
  switch (action.type) {
    case CourseActionTypes.LoadSuccess: {
      return {
        ...state,
        entities: [
          ...state.entities,
          ...action.payload
        ],
      };
    }

    case CourseActionTypes.LoadAuthorsSuccess: {
      return {
        ...state,
        authors: [
            ...action.payload
        ],
      };
    }

    case CourseActionTypes.Search: {
      return {
        ...state,
        searchValue: action.payload,
      };
    }

    case CourseActionTypes.Edit: {
      return {
        ...state,
        selectedId: action.payload,
      };
    }

    case CourseActionTypes.UpdateSuccess: {
      return {
        ...state,
        selectedId: undefined,
      };
    }

    case CourseActionTypes.SearchSuccess: {
      return {
        ...state,
        entities: action.payload,
      };
    }

    default: {
      return state;
    }
  }
}

// Course Selectors
export const getCourses: Selector<State, Course[]> = (state: State) => state.entities;
export const getAuthors: Selector<State, Author[]> = (state: State) => state.authors;
export const getSearchValue: Selector<State, string> = (state: State) => state.searchValue;
export const getSelectedId: Selector<State, number> = (state: State) => state.selectedId;
export const getCourseById: Selector<State, Course> = createSelector(
  getCourses,
  getSelectedId,
  (state: Course[], id: number) => {
      return state.find(item => item.id === id);
  }
);
