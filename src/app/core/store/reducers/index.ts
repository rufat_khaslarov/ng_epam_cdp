import { ActionReducer, ActionReducerMap, createSelector, MetaReducer, Selector } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';
import { User, Course, Author } from '../../../shared/models/index';

import * as fromAuth from './auth.reducer';
import * as fromCourse from './course.reducer';

export interface AppState {
  auth: fromAuth.State;
  course: fromCourse.State;
}

export const reducers: ActionReducerMap<AppState> = {
  auth: fromAuth.reducer,
  course: fromCourse.reducer,
};

export function logger(reducer: ActionReducer<AppState>): ActionReducer<AppState> {
  return function(state: AppState, action: any): AppState {
    console.log('STATE:', state);
    console.log('ACTION:', action);
    return reducer(state, action);
  };
}

export const metaReducers: MetaReducer<AppState>[] = [storeFreeze, logger];

// Global Selectors
export const getAuthState: Selector<AppState, fromAuth.State> = (state: AppState) => state.auth;
export const getAuthStatus: Selector<AppState, boolean> = createSelector(getAuthState, fromAuth.getLoggedIn);
export const getAuthUser: Selector<AppState, User> = createSelector(getAuthState, fromAuth.getUser);

export const getCourseState: Selector<AppState, fromCourse.State> = (state: AppState) => state.course;
export const getCoursesList: Selector<AppState, Course[]> = createSelector(getCourseState, fromCourse.getCourses);
export const getCoursesAuthorsList: Selector<AppState, Author[]> = createSelector(getCourseState, fromCourse.getAuthors);
export const getCoursesSearchValue: Selector<AppState, string> = createSelector(getCourseState, fromCourse.getSearchValue);
export const getCoursesSelectedItem: Selector<AppState, Course> = createSelector(getCourseState, fromCourse.getCourseById);
