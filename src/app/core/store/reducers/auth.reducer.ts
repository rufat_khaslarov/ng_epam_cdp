import { User } from '../../../shared/models/index';
import { AuthActionTypes, AuthActions } from '../actions/auth.actions';
import {Selector} from '@ngrx/store';

export interface State {
  token: string;
  user: User;
}

export const initialState: State = {
  token: '',
  user: undefined,
};

export function reducer(state: State = initialState, action: AuthActions): State {
  switch (action.type) {
    case AuthActionTypes.LoginSuccess: {
      return {
        ...state,
        token: action.payload.user.fakeToken,
        user: action.payload.user
      };
    }

    case AuthActionTypes.Logout: {
      return initialState;
    }

    default: {
      return state;
    }
  }
}

// Auth Selectors
export const getLoggedIn: Selector<State, boolean> = (state: State) => !!state.token;
export const getUser: Selector<State, User> = (state: State) => state.user;
