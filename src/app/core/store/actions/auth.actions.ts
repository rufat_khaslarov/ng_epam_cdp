import { Action } from '@ngrx/store';
import { User, Authenticate } from '../../../shared/models/index';

export enum AuthActionTypes {
  Login = '[User] Login',
  Logout = '[User] Logout',
  LoginSuccess = '[User] LoginSuccess',
  LoginRedirect = '[User] LoginRedirect',
}

export class Login implements Action {
  public readonly type: AuthActionTypes.Login = AuthActionTypes.Login;
  public payload: Authenticate;

  constructor(payload: Authenticate) {
    this.payload = payload;
  }
}

export class Logout implements Action {
  public readonly type: AuthActionTypes.Logout = AuthActionTypes.Logout;
}

export class LoginSuccess implements Action {
  public readonly type: AuthActionTypes.LoginSuccess = AuthActionTypes.LoginSuccess;
  public payload: { user: User };

  constructor(payload: { user: User }) {
    this.payload = payload;
  }
}

export class LoginRedirect implements Action {
  public readonly type: AuthActionTypes.LoginRedirect = AuthActionTypes.LoginRedirect;
}

export type AuthActions =
  | Login
  | Logout
  | LoginRedirect
  | LoginSuccess;
