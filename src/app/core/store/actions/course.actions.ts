import { Action } from '@ngrx/store';
import { Course, CoursePagination, Author } from '../../../shared/models/index';

export enum CourseActionTypes {
  Load = '[Course] Load',
  LoadAuthors = '[Course] LoadAuthors',
  Edit = '[Course] Edit',
  LoadSuccess = '[Course] LoadSuccess',
  LoadAuthorsSuccess = '[Course] LoadAuthorsSuccess',
  Search = '[Course] Search',
  SearchSuccess = '[Course] SearchSuccess',
  Delete = '[Course] Delete',
  DeleteSuccess = '[Course] DeleteSuccess',
  Update = '[Course] Update',
  UpdateSuccess = '[Course] UpdateSuccess',
}

export class Load implements Action {
  public readonly type: CourseActionTypes.Load = CourseActionTypes.Load;
  constructor(public payload: CoursePagination) {}
}

export class LoadAuthors implements Action {
  public readonly type: CourseActionTypes.LoadAuthors = CourseActionTypes.LoadAuthors;
}

export class Edit implements Action {
  public readonly type: CourseActionTypes.Edit = CourseActionTypes.Edit;
  constructor(public payload: number) {}
}

export class Delete implements Action {
  public readonly type: CourseActionTypes.Delete = CourseActionTypes.Delete;
  constructor(public payload: number) {}
}

export class Search implements Action {
  public readonly type: CourseActionTypes.Search = CourseActionTypes.Search;
  constructor(public payload: string) {}
}

export class Update implements Action {
  public readonly type: CourseActionTypes.Update = CourseActionTypes.Update;
  constructor(public payload: Course) {}
}

export class LoadSuccess implements Action {
  public readonly type: CourseActionTypes.LoadSuccess = CourseActionTypes.LoadSuccess;
  constructor(public payload: Course[]) {}
}

export class LoadAuthorsSuccess implements Action {
  public readonly type: CourseActionTypes.LoadAuthorsSuccess = CourseActionTypes.LoadAuthorsSuccess;
  constructor(public payload: Author[]) {}
}

export class SearchSuccess implements Action {
  public readonly type: CourseActionTypes.SearchSuccess = CourseActionTypes.SearchSuccess;
  constructor(public payload: Course[]) {}
}

export class DeleteSuccess implements Action {
  public readonly type: CourseActionTypes.DeleteSuccess = CourseActionTypes.DeleteSuccess;
}

export class UpdateSuccess implements Action {
  public readonly type: CourseActionTypes.UpdateSuccess = CourseActionTypes.UpdateSuccess;
}

export type CourseActions =
  | Load
  | LoadSuccess
  | Search
  | SearchSuccess
  | Delete
  | DeleteSuccess
  | Update
  | UpdateSuccess
  | Edit
  | LoadAuthors
  | LoadAuthorsSuccess;
