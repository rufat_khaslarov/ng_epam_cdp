import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { AuthService } from '../services';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  private authService: AuthService;

  constructor(authService: AuthService) {
    this.authService = authService;
  }

  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const tokenizedRequest: HttpRequest<any> = request.clone({
      headers: request.headers.set('Authorization', this.authService.getToken())
    });

    return next.handle(tokenizedRequest);
  }
}
