import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { LoadingService } from '../services';
import { Observable } from 'rxjs/internal/Observable';
import { finalize, delay } from 'rxjs/operators';

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {

  private loadingService: LoadingService;

  constructor(loadingService: LoadingService) {
    this.loadingService = loadingService;
  }

  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    this.loadingService.setStatus(true);

    return next.handle(request).pipe(
      delay(1000),
      finalize(() => {
        this.loadingService.setStatus(false);
      })
    );
  }
}
