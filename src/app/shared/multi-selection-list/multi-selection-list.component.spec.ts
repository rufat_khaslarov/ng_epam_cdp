import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiSelectionListComponent } from './multi-selection-list.component';

describe('MultiSelectionListComponent', () => {
  let component: MultiSelectionListComponent;
  let fixture: ComponentFixture<MultiSelectionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiSelectionListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiSelectionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
