import { Component } from '@angular/core';
import { LoadingService } from '../../core/services';

@Component({
  selector: 'spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent {

  public loadingService: LoadingService;

  constructor(loadingService: LoadingService) {
    this.loadingService = loadingService;
  }

}
