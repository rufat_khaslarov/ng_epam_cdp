import { ElementRef } from '@angular/core';
import { CourseStatusDirective } from './course-status.directive';

describe('CourseStatusDirective', () => {
  it('should create an instance', () => {
    const el: ElementRef = {
      nativeElement: {}
    };
    const directive: CourseStatusDirective = new CourseStatusDirective(el);
    expect(directive).toBeTruthy();
  });
});
