import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[regExpValidator]',
  providers: [
   {
     provide: NG_VALIDATORS,
     useValue: regExpValidator,
     multi: true
   }
 ]
})
export class RegExpValidatorDirective {}

export function regExpValidator(pattern: string): any {
    return function (control: AbstractControl): { [key: string]: any } {
        const value: string = control.value;
        const regExp: RegExp = new RegExp(pattern);
        console.log(value, pattern, regExp.test(value))
        return regExp.test(value) ? undefined : { notmatch: { value } };
    };
}
