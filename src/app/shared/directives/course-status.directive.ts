import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { Course } from '../models';

const twoWeeksInMs: number = 12096e5;

@Directive({
  selector: '[courseStatus]'
})
export class CourseStatusDirective implements OnInit {

  private el: ElementRef;

  @Input()
  public courseStatus: Course;

  constructor(el: ElementRef) {
    this.el = el;
  }

  public ngOnInit(): void {
    const now: number = Date.now();
    const twoWeeksBefore: Date = new Date(Date.now() - twoWeeksInMs);
    const createdAt: number = this.courseStatus.date.getTime();

    if (createdAt < now && createdAt >= twoWeeksBefore.getTime()) {
      this.el.nativeElement.style.border = '2px solid green';
    } else if (createdAt > now) {
      this.el.nativeElement.style.border = '1px solid #ea5455';
    }
  }

}
