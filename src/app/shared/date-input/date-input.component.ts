import { Component, EventEmitter, Input, Output, OnInit, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { formatDate } from '@angular/common';

@Component({
  selector: 'date-input',
  templateUrl: './date-input.component.html',
  styleUrls: ['./date-input.component.css'],
  providers: [
   {
     provide: NG_VALUE_ACCESSOR,
     useExisting: forwardRef(() => DateInputComponent),
     multi: true
   }
 ]
})
export class DateInputComponent implements ControlValueAccessor {

  @Input()
  public name: string = 'default';
  @Input()
  public invalid: boolean = false;
  public value: string = '';
  public _onChange: (_: any) => void;

  public onChange(event: Event): void {
    const value: string = (event.target as HTMLInputElement).value;
    this.value = formatDate(value, 'dd/MM/yyyy', 'en-US');
    this._onChange(this.value);
  }

  public registerOnTouched(fn: any): void {
      return;
  }

  public registerOnChange(fn: any): void {
      this._onChange = fn;
  }

  public writeValue(value: string = ''): void {
      this.value = value;
  }
}
