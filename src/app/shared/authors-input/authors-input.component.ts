import { Component, Input, Output, OnInit, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Author, AuthorTagModel } from '../models';

@Component({
  selector: 'authors-input',
  templateUrl: './authors-input.component.html',
  styleUrls: ['./authors-input.component.scss'],
  providers: [
   {
     provide: NG_VALUE_ACCESSOR,
     useExisting: forwardRef(() => AuthorsInputComponent),
     multi: true
   }
 ]
})
export class AuthorsInputComponent implements ControlValueAccessor {

    @Input()
    public authors: Author[] = [];
    @Input()
    public invalid: boolean = false;
    public value: AuthorTagModel[] = [];
    public _onChange: (_: any) => void;

    public getAutoCompleteValues(): string[] {
        return this.authors.map(author => author.name);
    }

    public onChange(data: AuthorTagModel[]): void {
        if (data && data.length) {
            this.value = data;
            this._onChange(this.authors.filter((author) => {
                return this.value.findIndex(v => v.value === author.name) !== -1;
            }));
        }
    }

    public registerOnTouched(fn: any): void {
        return;
    }

    public registerOnChange(fn: any): void {
        this._onChange = fn;
    }

    public writeValue(value: AuthorTagModel[]): void {
        this.value = value;
    }
}
