import { Component, EventEmitter, Input, Output, OnInit, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'duration-input',
  templateUrl: './duration-input.component.html',
  styleUrls: ['./duration-input.component.css'],
  providers: [
   {
     provide: NG_VALUE_ACCESSOR,
     useExisting: forwardRef(() => DurationInputComponent),
     multi: true
   }
 ]
})
export class DurationInputComponent implements ControlValueAccessor {

  @Input()
  public name: string = 'default';
  @Input()
  public value: string = '';
  @Input()
  public invalid: boolean = false;
  public _onChange: (_: any) => void;

  public onChange(event: Event): void {
    this.value = (event.target as HTMLInputElement).value;
    this._onChange(this.value);
  }

  public registerOnTouched(fn: any): void {
      return;
  }

  public registerOnChange(fn: any): void {
      this._onChange = fn;
  }

  public writeValue(value: string = ''): void {
      this.value = value;
  }
}
