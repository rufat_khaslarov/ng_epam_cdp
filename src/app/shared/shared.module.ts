import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatDurationPipe } from './pipes/format-duration.pipe';
import { OrderByDatePipe } from './pipes/order-by-date.pipe';
import { CourseStatusDirective } from './directives/course-status.directive';
import { FilterByNamePipe } from './pipes/filter-by-name.pipe';
import { ConfirmationModalComponent } from './confirmation-modal/confirmation-modal.component';
import { ModalModule } from 'ngx-bootstrap';
import { DateInputComponent } from './date-input/date-input.component';
import { DurationInputComponent } from './duration-input/duration-input.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpErrorComponent } from './http-error/http-error.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { CoreModule } from '../core/core.module';
import { RegExpValidatorDirective } from './directives/regexp-validator.directive';
import { AuthorsInputComponent } from './authors-input/authors-input.component';
import { TagInputModule } from 'ngx-chips';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CoreModule,
    ModalModule.forRoot(),
    ReactiveFormsModule,
    TagInputModule
  ],
  declarations: [
    FormatDurationPipe,
    OrderByDatePipe,
    CourseStatusDirective,
    FilterByNamePipe,
    ConfirmationModalComponent,
    DateInputComponent,
    DurationInputComponent,
    HttpErrorComponent,
    SpinnerComponent,
    RegExpValidatorDirective,
    AuthorsInputComponent
  ],
  exports: [
    FormatDurationPipe,
    OrderByDatePipe,
    CourseStatusDirective,
    ConfirmationModalComponent,
    DateInputComponent,
    DurationInputComponent,
    HttpErrorComponent,
    SpinnerComponent,
    RegExpValidatorDirective,
    AuthorsInputComponent
  ],
  entryComponents: [
    ConfirmationModalComponent
  ]
})
export class SharedModule { }
