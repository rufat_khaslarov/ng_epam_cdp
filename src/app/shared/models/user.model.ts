interface UserName {
  first: string;
  last: string;
}

export interface User {
  id: string;
  name: UserName;
  login: string;
  password: string;
  fakeToken: string;
}

export interface Authenticate {
  login: string;
  password: string;
}

export interface AuthToken {
  token: string;
}
