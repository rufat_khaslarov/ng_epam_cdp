export interface Course {
  id: number;
  name: string;
  description: string;
  date: Date;
  length: number;
  isTopRated: boolean;
  authors: Author[];
}

export interface CoursePagination {
  page: number;
  perPage: number;
}

export interface Author {
    id: string;
    name: string;
}

export interface AuthorTagModel {
   value: string;
   display: string;
}
