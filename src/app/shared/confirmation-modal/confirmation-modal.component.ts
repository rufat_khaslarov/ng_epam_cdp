import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.css']
})
export class ConfirmationModalComponent {

  public bsModalRef: BsModalRef;
  public title: string = 'Confirmation modal';
  public closeBtnName: string = 'Close';
  public confirmBtnName: string = 'Confirm';
  public onConfirm: () => void;

  constructor(bsModalRef: BsModalRef) {
    this.bsModalRef = bsModalRef;
  }

  public confirm(): void {
    if (this.onConfirm) {
      this.onConfirm();
    }
    this.bsModalRef.hide();
  }
}
