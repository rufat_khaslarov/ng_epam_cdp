import { Pipe, PipeTransform } from '@angular/core';
import { Course } from '../models';

@Pipe({
  name: 'filterByName'
})
export class FilterByNamePipe implements PipeTransform {
  public transform(courses: Course[], name: string): Course[] {
    return courses.filter(course => course && course.name.match(new RegExp(name, 'ig')));
  }
}
