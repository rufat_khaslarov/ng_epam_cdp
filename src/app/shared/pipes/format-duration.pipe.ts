import { Pipe, PipeTransform } from '@angular/core';

const minutesInHour: number = 60;

@Pipe({
  name: 'formatDuration'
})
export class FormatDurationPipe implements PipeTransform {

  public transform(value: number): string {

    const hours: number = Math.floor(value / minutesInHour);
    const minutes: number = value % minutesInHour;

    if (!hours) {
      return `${minutes}min`;
    }

    return `${hours}h ${minutes}min`;
  }

}
