import { Pipe, PipeTransform } from '@angular/core';
import { Course } from '../models';

@Pipe({
  name: 'orderByDate'
})
export class OrderByDatePipe implements PipeTransform {

  public transform(courses: Array<Course>, args?: any): Array<Course> {
    return courses.sort((a, b) => a.date.getTime() - b.date.getTime());
  }

}
