import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { CourseRoutingModule } from '../course/course-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CourseRoutingModule
  ],
  declarations: [
      BreadcrumbsComponent
  ],
  exports: [
      BreadcrumbsComponent
  ]
})
export class BreadcrumbsModule {}
