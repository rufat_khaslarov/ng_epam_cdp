import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRoute, RoutesRecognized } from '@angular/router';
import { CoursesService } from '../../core/services';
import { RouterEvent } from '@angular/router/src/events';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit, OnDestroy {

  private router: Router;
  private route: ActivatedRoute;
  private coursesService: CoursesService;
  private routerSubscription: Subscription;
  private courseSubscription: Subscription;
  public currentCourseName: string = '';

  constructor(router: Router, route: ActivatedRoute, coursesService: CoursesService) {
    this.router = router;
    this.coursesService = coursesService;
    this.route = route;
  }

  private onParamsChange(data: RouterEvent): void {

    if (data instanceof RoutesRecognized) {

      const id: number = Number(data.state.root.firstChild.params.id);

      this.courseSubscription = this.coursesService.getCourseById(id)
        .subscribe(course => {
          this.currentCourseName = (course && course.name) || '';
        });
    }
  }

  public ngOnInit(): void {
    this.routerSubscription = this.router.events.subscribe((data: RouterEvent) => this.onParamsChange(data));
  }

  public ngOnDestroy(): void {
    this.routerSubscription.unsubscribe();
    this.courseSubscription.unsubscribe();
  }

}
