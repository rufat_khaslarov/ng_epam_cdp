import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/internal/Subscription';
import { debounceTime, filter } from 'rxjs/operators';
import { AppState } from '../../core/store/reducers';
import * as CourseActions from '../../core/store/actions/course.actions';

const SEARCH_DEBOUNCE_TIME: number = 500;

@Component({
  selector: 'search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit, OnDestroy {

    private searchSubscription: Subscription;
    public searchValue: FormControl = new FormControl('');

    constructor(
        private router: Router,
        private store: Store<AppState>
    ) {
    }

    public ngOnInit(): void {
        this.searchSubscription = this.searchValue.valueChanges.pipe(
            debounceTime(SEARCH_DEBOUNCE_TIME),
            filter(value => value && value.length >= 3)
        )
        .subscribe((value) => {
            this.store.dispatch(new CourseActions.Search(value));
        });
    }

    public onCourseAdd(): void {
      this.router.navigate(['courses/new']);
    }

    public ngOnDestroy(): void {
      this.searchSubscription.unsubscribe();
    }
}
