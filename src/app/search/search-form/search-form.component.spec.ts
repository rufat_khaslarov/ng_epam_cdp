import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

import { SearchFormComponent } from './search-form.component';
import {DebugElement} from '@angular/core';

describe('SearchFormComponent', () => {
  let component: SearchFormComponent;
  let fixture: ComponentFixture<SearchFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule ],
      declarations: [ SearchFormComponent ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should synchronize input value with searchText property', async(() => {
    fixture.whenStable().then(() => {
      let input: DebugElement = fixture.debugElement.query(By.css("input[type='text']"));
      let el: any = input.nativeElement;

      el.value = 'test';
      el.dispatchEvent(new Event('input'));

      expect(fixture.componentInstance.searchText).toBe('test');
    });
  }));

  it('should call onSearch by search button clicking', async(() => {
    spyOn(component, 'onSearch');

    let button: DebugElement = fixture.debugElement.query(By.css("input[type='submit']"));
    button.triggerEventHandler('click', undefined);

    fixture.whenStable().then(() => {
      expect(component.onSearch).toHaveBeenCalled();
    });

  }));

  it('should call console.log in onSearch', () => {
    spyOn(console, 'log');
    component.onSearch();
    expect(console.log).toHaveBeenCalled();
  });

});
