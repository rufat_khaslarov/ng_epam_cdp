import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as UserActions from '../../core/store/actions/auth.actions';
import { AppState } from '../../core/store/reducers';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent {
    public loginForm: FormGroup = new FormGroup({
      login: new FormControl(''),
      password: new FormControl(''),
    });

    constructor(
        private store: Store<AppState>
    ) {}

    public login(): void {
        this.store.dispatch(new UserActions.Login(this.loginForm.value));
    }
}
