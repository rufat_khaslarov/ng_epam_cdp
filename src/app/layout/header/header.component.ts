import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../../core/services';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';
import { Store, select } from '@ngrx/store';
import * as AuthActions from '../../core/store/actions/auth.actions';
import { getAuthUser, AppState } from '../../core/store/reducers';

@Component({
  selector: 'layout-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

    private authSubscription: Subscription;
    public isCollapsed: boolean = true;
    public username: string;

    constructor(
      private authService: AuthService,
      private router: Router,
      private store: Store<AppState>
    ) {}

    public ngOnInit(): void {
      this.authSubscription = this.store.pipe(
        select(getAuthUser)
      ).subscribe((user) => {
        this.username = user && user.name ? `${user.name.first} ${user.name.last}` : '';
      });
    }

    public ngOnDestroy(): void {
      this.authSubscription.unsubscribe();
    }

    public logOut(): void {
      this.store.dispatch(new AuthActions.Logout());
    }
}
