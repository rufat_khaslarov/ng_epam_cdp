import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LogoComponent } from './logo/logo.component';
import { CollapseModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    CollapseModule.forRoot()
  ],
  declarations: [
      HeaderComponent,
      FooterComponent,
      LogoComponent,
  ],
  exports: [
      HeaderComponent,
      FooterComponent,
      LogoComponent,
  ],
})
export class LayoutModule { }
