import { LogoComponent } from './logo.component';

describe('LogoComponent', () => {
  it('should have logoText property', () => {
    const comp: LogoComponent = new LogoComponent();
    expect(comp.logoText).toBeTruthy();
  });
});
