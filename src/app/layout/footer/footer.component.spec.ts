import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterComponent } from './footer.component';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterComponent ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it("should contain 'Copyright'", () => {
    const footerElement: HTMLElement = fixture.nativeElement;
    expect(footerElement.textContent).toContain('Copyright');
  });

  it('should contain footer element', () => {
    const footerElement: HTMLElement = fixture.nativeElement;
    expect(footerElement.querySelector('footer')).toBeTruthy();
  });
});
